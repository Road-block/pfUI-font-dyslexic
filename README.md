# pfUI-font-dyslexic

A font package for [pfUI](https://gitlab.com/shagu/pfUI), providing additional font options from [OpenDyslexic](https://opendyslexic.org/).  

## Installation
1. Download **[Latest Version](https://gitlab.com/Road-block/pfUI-font-dyslexic/-/archive/master/pfUI-font-dyslexic-master.zip)**
2. Unpack the Zip file
3. Rename the folder "pfUI-font-dyslexic-master" to "pfUI-font-dyslexic"
4. Copy "pfUI-font-dyslexic" into Wow-Directory\Interface\AddOns
5. Restart Wow

## Preview
![thumbnails](https://www.dafont.com/img/charmap/o/p/open_dyslexic8.png)

## Fonts
* OpenDyslexic-Regular
* OpenDyslexic-Italic
* OpenDyslexic-Bold
* OpenDyslexic-BoldItalic
* OpenDyslexicAlta-Regular
* OpenDyslexicAlta-Italic
* OpenDyslexicAlta-Bold
* OpenDyslexicAlta-BoldItalic
* OpenDyslexic3-Regular
* OpenDyslexic3-Bold
* OpenDyslexicMono-Regular