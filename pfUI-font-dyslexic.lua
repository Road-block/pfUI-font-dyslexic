--[[Font Source
https://opendyslexic.org/faq
]]

table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexic-Regular.ttf:OpenDyslexic-Regular")
table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexic-Bold.ttf:OpenDyslexic-Bold")
table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexic-Italic.ttf:OpenDyslexic-Italic")
table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexic-BoldItalic.ttf:OpenDyslexic-BoldItalic")

table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexicAlta-Regular.ttf:OpenDyslexicAlta-Regular")
table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexicAlta-Bold.ttf:OpenDyslexicAlta-Bold")
table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexicAlta-Italic.ttf:OpenDyslexicAlta-Italic")
table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexicAlta-BoldItalic.ttf:OpenDyslexicAlta-BoldItalic")

table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexic3-Regular.ttf:OpenDyslexic3-Regular")
table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexic3-Bold.ttf:OpenDyslexic3-Bold")

table.insert(pfUI.gui.dropdowns.fonts, "Interface\\AddOns\\pfUI-font-dyslexic\\fonts\\OpenDyslexicMono-Regular.ttf:OpenDyslexicMono-Regular")
